package com.app.helpdesk.controller;

import com.app.helpdesk.dto.FeedbackDto;
import com.app.helpdesk.model.Ticket;
import com.app.helpdesk.security.CustomUserDetails;
import com.app.helpdesk.service.FeedbackService;
import com.app.helpdesk.util.TicketAccessResolver;
import com.app.helpdesk.util.ValidatorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/feedbacks")
public class FeedbackController {

    private static final String URI_FOR_RESPONSE = "feedbacks/%s";

    private final FeedbackService feedbackService;
    private final ValidatorUtil validatorUtil;
    private final TicketAccessResolver ticketAccessResolver;

    @Autowired
    public FeedbackController(FeedbackService feedbackService, ValidatorUtil validatorUtil, TicketAccessResolver ticketAccessResolver) {
        this.feedbackService = feedbackService;
        this.validatorUtil = validatorUtil;
        this.ticketAccessResolver = ticketAccessResolver;
    }

    @PostMapping("/{ticketId}")
    public ResponseEntity<?> create(@Valid @RequestBody FeedbackDto feedbackDto,
                                    BindingResult bindingResult,
                                    @AuthenticationPrincipal CustomUserDetails userDetails,
                                    @PathVariable Long ticketId) {
        List<String> errorMessages = validatorUtil.generateErrorMessages(bindingResult);
        if (!errorMessages.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMessages);
        }
        Ticket ticket = ticketAccessResolver.checkAccessToFeedbackTicket(userDetails.getUser(), ticketId);
        feedbackService.save(feedbackDto, userDetails.getUser(), ticket);
        return ResponseEntity.created(URI.create(String.format(URI_FOR_RESPONSE, ticketId))).build();
    }

    @GetMapping("/{ticketId}")
    public ResponseEntity<List<FeedbackDto>> getAll(@AuthenticationPrincipal CustomUserDetails userDetails,
                                                    @PathVariable Long ticketId) {
        List<FeedbackDto> feedbacks = feedbackService.get(userDetails.getUser(), ticketId);
        return ResponseEntity.ok(feedbacks);
    }
}
