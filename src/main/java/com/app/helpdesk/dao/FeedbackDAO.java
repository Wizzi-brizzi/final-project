package com.app.helpdesk.dao;

import com.app.helpdesk.model.Feedback;

import java.util.List;

public interface FeedbackDAO {
    void save(Feedback feedback);

    List<Feedback> get(Long userId, Long ticketId);
}
