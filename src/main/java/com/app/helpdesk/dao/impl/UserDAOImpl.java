package com.app.helpdesk.dao.impl;

import com.app.helpdesk.dao.UserDAO;
import com.app.helpdesk.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

import static org.springframework.transaction.annotation.Propagation.MANDATORY;

@Repository
@Transactional(propagation = MANDATORY)
public class UserDAOImpl implements UserDAO {
    private final EntityManager entityManager;

    @Autowired
    public UserDAOImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public Optional<User> findUserByEmail(String email) {
        return entityManager.createQuery("from User where email = :email", User.class)
                .setParameter("email", email)
                .getResultStream()
                .findAny();
    }

    @Override
    public List<User> getAllManagers() {
        return entityManager.createQuery("from User where role = 'ROLE_MANAGER'", User.class)
                .getResultList();
    }

    @Override
    public List<User> getAllEngineer() {
        return entityManager.createQuery("from User where role = 'ROLE_ENGINEER'", User.class)
                .getResultList();
    }
}
