package com.app.helpdesk.service.impl;

import com.app.helpdesk.dao.CategoryDAO;
import com.app.helpdesk.dto.CategoryDto;
import com.app.helpdesk.exception.custom.NoSuchCategoryException;
import com.app.helpdesk.model.Category;
import com.app.helpdesk.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashSet;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryDAO categoryDAO;

    @Autowired
    public CategoryServiceImpl(CategoryDAO categoryDAO) {
        this.categoryDAO = categoryDAO;
    }

    @Override
    @Transactional(readOnly = true)
    public CategoryDto getAll() {
        return new CategoryDto(new LinkedHashSet<>(categoryDAO.getAll()));
    }

    @Override
    @Transactional(readOnly = true)
    public Category getByName(String categoryName) {
        return categoryDAO.getByName(categoryName)
                .orElseThrow(() -> new NoSuchCategoryException(String.format("Category with name: %s not found", categoryName)));
    }
}
