package com.app.helpdesk.model.enums;

public enum Urgency {
    CRITICAL,
    HIGH,
    AVERAGE,
    LOW
}
