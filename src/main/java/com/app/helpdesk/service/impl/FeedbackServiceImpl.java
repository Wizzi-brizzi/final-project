package com.app.helpdesk.service.impl;

import com.app.helpdesk.dao.FeedbackDAO;
import com.app.helpdesk.dto.FeedbackDto;
import com.app.helpdesk.mapper.FeedbackMapper;
import com.app.helpdesk.model.Feedback;
import com.app.helpdesk.model.Ticket;
import com.app.helpdesk.model.User;
import com.app.helpdesk.service.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class FeedbackServiceImpl implements FeedbackService {

    private final FeedbackDAO feedbackDAO;
    private final FeedbackMapper feedbackMapper;

    @Autowired
    public FeedbackServiceImpl(FeedbackDAO feedbackDAO, FeedbackMapper feedbackMapper) {
        this.feedbackDAO = feedbackDAO;
        this.feedbackMapper = feedbackMapper;
    }

    @Override
    @Transactional
    public void save(FeedbackDto feedbackDto, User user, Ticket ticket) {
        Feedback feedback = feedbackMapper.mapToEntity(feedbackDto, ticket, user);
        feedbackDAO.save(feedback);
    }

    @Override
    @Transactional(readOnly = true)
    public List<FeedbackDto> get(User user, Long ticketId) {
        List<Feedback> feedbacks = feedbackDAO.get(user.getId(), ticketId);
        return feedbackMapper.mapToDto(feedbacks);
    }
}
