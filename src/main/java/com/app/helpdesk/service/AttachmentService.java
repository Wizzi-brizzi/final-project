package com.app.helpdesk.service;

import com.app.helpdesk.model.Attachment;

public interface AttachmentService {
    Attachment getById(Long attachmentId);

    void deleteById(Long attachmentId);
}
