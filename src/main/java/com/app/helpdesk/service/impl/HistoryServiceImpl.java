package com.app.helpdesk.service.impl;

import com.app.helpdesk.dao.HistoryDAO;
import com.app.helpdesk.mapper.HistoryMapper;
import com.app.helpdesk.model.Attachment;
import com.app.helpdesk.model.History;
import com.app.helpdesk.model.Ticket;
import com.app.helpdesk.model.enums.State;
import com.app.helpdesk.service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class HistoryServiceImpl implements HistoryService {

    private final HistoryDAO historyDAO;
    private final HistoryMapper historyMapper;

    @Autowired
    public HistoryServiceImpl(HistoryDAO historyDAO, HistoryMapper historyMapper) {
        this.historyDAO = historyDAO;
        this.historyMapper = historyMapper;
    }

    @Override
    @Transactional
    public void saveAfterCreateTicket(Ticket ticket) {
        History history = historyMapper.getHistoryAfterTicketCreate(ticket);
        historyDAO.save(history);
    }

    @Override
    @Transactional
    public void saveAfterUpdateTicket(Ticket ticket) {
        History history = historyMapper.getHistoryAfterTicketUpdate(ticket);
        historyDAO.save(history);
    }

    @Override
    @Transactional
    public void saveAfterUpdateState(State previousState, Ticket ticket) {
        History history = historyMapper.getHistoryAfterTicketChangeState(previousState, ticket);
        historyDAO.save(history);
    }

    @Override
    @Transactional
    public void saveAfterCreateAttachment(Attachment attachment) {
        History history = historyMapper.getHistoryAfterAttachmentCreate(attachment);
        historyDAO.save(history);
    }

    @Override
    @Transactional
    public void saveAfterRemoveAttachment(Attachment attachment) {
        History history = historyMapper.getHistoryAfterAttachmentRemove(attachment);
        historyDAO.save(history);
    }
}
