package com.app.helpdesk.service;

import com.app.helpdesk.model.Attachment;
import com.app.helpdesk.model.Ticket;
import com.app.helpdesk.model.enums.State;

public interface HistoryService {
    void saveAfterCreateTicket(Ticket ticket);

    void saveAfterUpdateTicket(Ticket ticket);

    void saveAfterUpdateState(State previousState, Ticket ticket);

    void saveAfterCreateAttachment(Attachment attachment);

    void saveAfterRemoveAttachment(Attachment attachment);
}
