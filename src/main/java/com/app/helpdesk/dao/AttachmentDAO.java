package com.app.helpdesk.dao;

import com.app.helpdesk.model.Attachment;

import java.util.Optional;

public interface AttachmentDAO {
    Optional<Attachment> getById(Long attachmentId);

    void delete(Attachment attachment);

    Optional<Attachment> getForUserById(Long userId, Long attachmentId);
}
