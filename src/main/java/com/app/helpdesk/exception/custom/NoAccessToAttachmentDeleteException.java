package com.app.helpdesk.exception.custom;

public class NoAccessToAttachmentDeleteException extends RuntimeException{
    public NoAccessToAttachmentDeleteException(String message) {
        super(message);
    }
}
