package com.app.helpdesk.service.impl;

import com.app.helpdesk.dao.AttachmentDAO;
import com.app.helpdesk.exception.custom.AttachmentNotFoundException;
import com.app.helpdesk.model.Attachment;
import com.app.helpdesk.service.AttachmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class AttachmentServiceImpl implements AttachmentService {

    private final AttachmentDAO attachmentDAO;

    @Autowired
    public AttachmentServiceImpl(AttachmentDAO attachmentDAO) {
        this.attachmentDAO = attachmentDAO;
    }

    @Override
    @Transactional(readOnly = true)
    public Attachment getById(Long attachmentId) {
        return attachmentDAO.getById(attachmentId)
                .orElseThrow(() -> new AttachmentNotFoundException(String.format("Attachment with id : %s not found", attachmentId)));
    }

    @Override
    @Transactional
    public void deleteById(Long attachmentId) {
        Attachment attachment = getById(attachmentId);
        attachmentDAO.delete(attachment);
    }
}
