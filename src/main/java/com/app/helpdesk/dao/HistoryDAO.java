package com.app.helpdesk.dao;

import com.app.helpdesk.model.History;

public interface HistoryDAO {
    void save(History history);
}
