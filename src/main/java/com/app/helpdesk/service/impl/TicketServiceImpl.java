package com.app.helpdesk.service.impl;

import com.app.helpdesk.dao.TicketDAO;
import com.app.helpdesk.dto.CategoryDto;
import com.app.helpdesk.dto.TicketDto;
import com.app.helpdesk.dto.TicketDtoWrapper;
import com.app.helpdesk.exception.custom.TicketNotFoundException;
import com.app.helpdesk.mapper.TicketMapper;
import com.app.helpdesk.model.Ticket;
import com.app.helpdesk.model.User;
import com.app.helpdesk.model.enums.State;
import com.app.helpdesk.service.CategoryService;
import com.app.helpdesk.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import static com.app.helpdesk.model.enums.Role.*;

@Service
public class TicketServiceImpl implements TicketService {

    private static final Map<String, Comparator<Ticket>> TICKET_SORTERS = Map.of(
            "id-asc", Comparator.comparing(Ticket::getId),
            "id-desc", Comparator.comparing(Ticket::getId, Comparator.reverseOrder()),
            "name-asc", Comparator.comparing(Ticket::getName),
            "name-desc", Comparator.comparing(Ticket::getName, Comparator.reverseOrder()),
            "desireddate-asc", Comparator.comparing(Ticket::getDesiredResolutionDate),
            "desireddate-desc", Comparator.comparing(Ticket::getDesiredResolutionDate, Comparator.reverseOrder()),
            "urgency-asc", Comparator.comparing(Ticket::getUrgency),
            "urgency-desc", Comparator.comparing(Ticket::getUrgency, Comparator.reverseOrder()),
            "status-asc", Comparator.comparing(Ticket::getState),
            "status-desc", Comparator.comparing(Ticket::getState, Comparator.reverseOrder())
    );
    private static final String TICKET_ERROR_MESSAGE = "Ticket with id %s not found";

    private final TicketDAO ticketDAO;
    private final TicketMapper ticketMapper;
    private final CategoryService categoryService;

    @Autowired
    public TicketServiceImpl(TicketDAO ticketDAO, TicketMapper ticketMapper, CategoryService categoryService) {
        this.ticketDAO = ticketDAO;
        this.ticketMapper = ticketMapper;
        this.categoryService = categoryService;
    }

    @Override
    @Transactional(readOnly = true)
    public TicketDtoWrapper getAllTicketsForCurrentUser(User user, String sortParam, int amountTicketsAtPage, int page) {

        List<Ticket> allTickets = new ArrayList<>();
        long amountTickets = 0;

        if (user.getRole().equals(ROLE_EMPLOYEE)) {
            allTickets = ticketDAO.getForEmployee(user.getId(), amountTicketsAtPage, page);
            amountTickets = ticketDAO.getAmountOwnForEmployee(user.getId());
        }

        if (user.getRole().equals(ROLE_MANAGER)) {
            allTickets = ticketDAO.getAllForManager(user.getId(), amountTicketsAtPage, page);
            amountTickets = ticketDAO.getAmountAllForManager(user.getId());
        }

        if (user.getRole().equals(ROLE_ENGINEER)) {
            allTickets = ticketDAO.getAllForEngineer(user.getId(), amountTicketsAtPage, page);
            amountTickets = ticketDAO.getAmountAllForEngineer(user.getId());
        }

        sortTicketsByParam(allTickets, sortParam);

        List<TicketDto> ticketsDto = ticketMapper.mapToDto(allTickets);
        return new TicketDtoWrapper(ticketsDto, amountTickets);
    }

    @Override
    @Transactional(readOnly = true)
    public TicketDtoWrapper getOwnTicketsForCurrentUser(User user, String sortParam, int amountTicketsAtPage, int page) {

        List<Ticket> ownTickets = new ArrayList<>();
        long amountTickets = 0;

        if (user.getRole().equals(ROLE_EMPLOYEE)) {
            ownTickets = ticketDAO.getForEmployee(user.getId(), amountTicketsAtPage, page);
            amountTickets = ticketDAO.getAmountOwnForEmployee(user.getId());
        }

        if (user.getRole().equals(ROLE_MANAGER)) {
            ownTickets = ticketDAO.getOwnForManager(user.getId(), amountTicketsAtPage, page);
            amountTickets = ticketDAO.getAmountOwnForManager(user.getId());
        }

        if (user.getRole().equals(ROLE_ENGINEER)) {
            ownTickets = ticketDAO.getAllForEngineer(user.getId(), amountTicketsAtPage, page);
            amountTickets = ticketDAO.getAmountAllForEngineer(user.getId());
        }

        sortTicketsByParam(ownTickets, sortParam);

        List<TicketDto> ticketsDto = ticketMapper.mapToDto(ownTickets);
        return new TicketDtoWrapper(ticketsDto, amountTickets);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TicketDto> filter(User user, String filterRequest) {

        List<Ticket> filteredTickets = new ArrayList<>();

        if (user.getRole().equals(ROLE_EMPLOYEE)) {
            filteredTickets = ticketDAO.filterForEmployee(filterRequest, user.getId());
        }

        if (user.getRole().equals(ROLE_MANAGER)) {
            filteredTickets = ticketDAO.filterForManager(filterRequest, user.getId());
        }

        if (user.getRole().equals(ROLE_ENGINEER)) {
            filteredTickets = ticketDAO.filterForEngineer(filterRequest, user.getId());
        }

        return ticketMapper.mapToDto(filteredTickets);
    }

    @Override
    @Transactional
    public void saveOrUpdate(TicketDto ticketDto, MultipartFile file, User user, String draft) {
        Ticket ticket = ticketMapper.mapToEntity(ticketDto, file, user, draft);
        if (ticket.getId() != null) {
            ticketDAO.update(ticket);
        } else {
            ticketDAO.save(ticket);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public TicketDto getTicketDtoById(Long ticketId) {
        Ticket ticket = getById(ticketId);
        return ticketMapper.mapToDto(ticket);
    }

    @Override
    public Ticket getById(Long ticketId) {
        return ticketDAO.getById(ticketId)
                .orElseThrow(() -> new TicketNotFoundException(String.format(TICKET_ERROR_MESSAGE, ticketId)));
    }

    @Override
    @Transactional(readOnly = true)
    public TicketDto getDraft(Long ticketId) {
        CategoryDto categories = categoryService.getAll();
        Ticket ticket = ticketDAO.getById(ticketId)
                .orElseThrow(() -> new TicketNotFoundException(String.format(TICKET_ERROR_MESSAGE, ticketId)));
        return ticketMapper.mapToDraftDto(ticket, categories);
    }

    @Override
    @Transactional
    public void changeState(User user, Long ticketId, State newState) {
        Ticket ticket = ticketDAO.getById(ticketId)
                .orElseThrow(() -> new TicketNotFoundException(String.format(TICKET_ERROR_MESSAGE, ticketId)));

        ticket.setState(newState);

        if (newState == State.APPROVED) {
            ticket.setApprover(user);
        } else if (newState == State.IN_PROGRESS) {
            ticket.setAssignee(user);
        }
        ticketDAO.changeState(ticket);
    }

    private void sortTicketsByParam(List<Ticket> tickets, String sortParam) {
        tickets.sort(TICKET_SORTERS.getOrDefault(sortParam, Comparator.comparing(Ticket::getUrgency)
                .thenComparing(Ticket::getDesiredResolutionDate, Comparator.reverseOrder())));
    }
}
