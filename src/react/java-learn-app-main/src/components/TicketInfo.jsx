import React from "react";
import PropTypes from "prop-types";
import CommentsTable from "./CommentsTable";
import HistoryTable from "./HistoryTable";
import TabPanel from "./TabPanel";
import TicketCreationPageWithRouter from "./TicketCreationPage";
import { Link, Route, Switch } from "react-router-dom";
import { withRouter } from "react-router";
import { ALL_TICKETS } from "../constants/mockTickets";
import { COMMENTS } from "../constants/mockComments";
import { HISTORY } from "../constants/mockHistory";

import {
  Button,
  ButtonGroup,
  Paper,
  Tab,
  Tabs,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Typography,
  TextField,
} from "@material-ui/core";

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    "aria-controls": `full-width-tabpanel-${index}`,
  };
}

class TicketInfo extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      commentValue: "",
      tabValue: 0,
      commentCreationError : [],
      ticketComments: null,
      ticketHistory: null,
      currentUser: {
        name: "Dave Brubeck",
        id: 4242,
      },
      ticketData: {
        id: 42,
        name: "Something",
        date: "2021-07-16",
        category: "Hardware & Software",
        status: "New",
        urgency: "High",
        resolutionDate: "",
        ticketOwner: "Robert Oppenheimer",
        approver: "",
        assignee: "",
        attachment: null,
        description: "Desc",
      },
    };
  }

  componentDidMount() {
    const { ticketId } = this.props.match.params;

    const request = {
      method: 'GET',
      headers: {
        'Authorization': localStorage.getItem("token")
      }
    }

    const url = 'http://localhost:8080/tickets/' + ticketId;


    fetch(url, request)
      .then(data => data.json())
      .then(data => {
        console.log(data);
        this.setState({
          ticketData: {
            ...this.state.ticketData,
            id: data.id,
            date: data.createdOn,
            resolutionDate: data.resolutionDate,
            name: data.name,
            status: data.status,
            urgency: data.urgency,
            action: data.action,
            category: data.category,
            ticketOwner: data.owner,
            approver: data.approver,
            assignee: data.assignee,
            attachment: data.attachment,
            description: data.description,
          },
          ticketHistory: data.histories,
          ticketComments: data.comments,
        });
      });
  }

  handleDownloadAttachment = () => {
    const {
      attachment,
    } = this.state.ticketData;

    const request = {
      method: 'GET',
      headers: {
        'Authorization': localStorage.getItem("token")
      }
    }

    const url = 'http://localhost:8080/attachments/' + attachment.id;

    fetch(url, request)
      .then((response) => {
        response.blob()
      })
      .then((blob) => {
        const url = window.URL.createObjectURL(
          new Blob([blob]),
        );
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute(
          'download',
          `FileName.jpg`,
        );
        document.body.appendChild(link);
        link.click();
        link.parentNode.removeChild(link);
      });
  }


  handleTabChange = (event, value) => {
    this.setState({
      tabValue: value,
    });
  };

  handleEnterComment = (event) => {
    this.setState({
      commentValue: event.target.value,
    });
  };

  addComment = () => {

    const ticketFromUrl = window.location.href.split("/");
    const ticketIdFromUrl = ticketFromUrl[ticketFromUrl.length - 1];



    const requestBody = { text: this.state.commentValue }

    const request = {
      method: 'POST',
      body: JSON.stringify(requestBody),
      headers: {
        'Authorization': localStorage.getItem("token"),
        'Content-Type': 'application/json;charset=utf-8'
      }
    }
    const url = 'http://localhost:8080/comments/' + ticketIdFromUrl;

    fetch(url, request)
      .then(data => {
        if (data.status === 201) {
          this.setState({
            commentValue: "",
          });
        } else {

          this.setState({
            commentCreationError : data.json()
          })

        }
      })

  };

  handleSubmitTicket = () => {
    console.log("SUBMIT ticket");
  };

  handleEditTicket = () => {
    console.log("EDIT ticket");
  };

  handleCancelTicket = () => {
    // set ticket status to 'canceled' status
    console.log("CANCEL ticket");
  };

  handleDraftTicket = (e) => {
    console.log('DRAFT ' + this.state.ticketData.id);
  }

  render() {
    const {
      approver,
      id,
      name,
      date,
      category,
      status,
      urgency,
      resolutionDate,
      ticketOwner,
      assignee,
      attachment,
      description,
    } = this.state.ticketData;

    const { commentValue, tabValue, ticketComments, ticketHistory, commentCreationError } =
      this.state;

    const { url } = this.props.match;

    const { handleCancelTicket, handleEditTicket, handleSubmitTicket } = this;

    return (
      <Switch>
        <Route exact path={url}>
          <div className="ticket-data-container">
            <div className={"ticket-data-container__back-button back-button"}>
              <Button component={Link} to="/tickets" variant="contained">
                Ticket list
              </Button>
            </div>
            <div className="ticket-data-container__title">
              <Typography variant="h4">{`Ticket(${id}) - ${name}`}</Typography>
            </div>
            <div className="ticket-data-container__info">
              <TableContainer className="ticket-table" component={Paper}>
                <Table>
                  <TableBody>
                    <TableRow>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          Created on:
                        </Typography>
                      </TableCell>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          {date}
                        </Typography>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          Category:
                        </Typography>
                      </TableCell>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          {category}
                        </Typography>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          Status:
                        </Typography>
                      </TableCell>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          {status}
                        </Typography>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          Urgency:
                        </Typography>
                      </TableCell>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          {urgency}
                        </Typography>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          Desired Resolution Date:
                        </Typography>
                      </TableCell>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          {resolutionDate}
                        </Typography>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          Owner:
                        </Typography>
                      </TableCell>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          {ticketOwner}
                        </Typography>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          Approver:
                        </Typography>
                      </TableCell>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          {approver || "Not assigned"}
                        </Typography>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          Assignee:
                        </Typography>
                      </TableCell>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          {assignee || "Not assigned"}
                        </Typography>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          Attachments:
                        </Typography>
                      </TableCell>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          {attachment == null ? "Not assigned" : <button onClick={() => this.handleDownloadAttachment()}>{attachment.attachmentName}  Download</button>}
                        </Typography>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          Description:
                        </Typography>
                      </TableCell>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          {description || "Not assigned"}
                        </Typography>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>

                {status === 'DRAFT' ?
                  <div className={"ticket-data-container__back-button back-button"}>
                    <Button component={Link} to={`/tickets-draft/${id}`} variant="contained">
                      DRAFT
                    </Button>
                  </div> : ''}

              </TableContainer>
            </div>
            {status === "draft" && (
              <div className="ticket-data-container__button-section">
                <ButtonGroup variant="contained" color="primary">
                  <Button
                    component={Link}
                    to="/tickets"
                    onClick={handleSubmitTicket}
                  >
                    Submit
                  </Button>

                  <Button
                    component={Link}
                    to={`/create-ticket/${id}`}
                    onClick={handleEditTicket}
                  >
                    Edit
                  </Button>
                  <Button
                    component={Link}
                    to="/main-page"
                    onClick={handleCancelTicket}
                  >
                    Cancel
                  </Button>
                </ButtonGroup>
              </div>
            )}
            <div className="ticket-data-container__comments-section comments-section">
              <div className="">
                <Tabs
                  variant="fullWidth"
                  onChange={this.handleTabChange}
                  value={tabValue}
                  indicatorColor="primary"
                  textColor="primary"
                >
                  <Tab label="History" {...a11yProps(0)} />
                  <Tab label="Comments" {...a11yProps(1)} />
                </Tabs>
                <TabPanel value={tabValue} index={0}>
                  <HistoryTable history={ticketHistory} />
                </TabPanel>
                <TabPanel value={tabValue} index={1}>
                  <CommentsTable comments={ticketComments} />
                </TabPanel>
              </div>
              {
                    commentCreationError.length > 0 ?

                        <div>
                            <ol>
                                {commentCreationError.map((key, value) => {
                                    return <li key={key}>{key} {value}</li>
                                })}
                            </ol>
                        </div>
                        :
                        ''
                }
            </div>
            {tabValue && (
              <div className="ticket-data-container__enter-comment-section enter-comment-section">
                <TextField
                  label="Enter a comment"
                  multiline
                  rows={4}
                  value={commentValue}
                  variant="filled"
                  className="comment-text-field"
                  onChange={this.handleEnterComment}
                />
                <div className="enter-comment-section__add-comment-button">
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={this.addComment}
                  >
                    Add Comment
                  </Button>
                </div>
              </div>
            )}
          </div>
        </Route>
        <Route path="/create-ticket/:ticketId">
          <TicketCreationPageWithRouter />
        </Route>
      </Switch>
    );
  }
}

TicketInfo.propTypes = {
  match: PropTypes.object,
};

const TicketInfoWithRouter = withRouter(TicketInfo);
export default TicketInfoWithRouter;
