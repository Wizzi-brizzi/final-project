package com.app.helpdesk.dao;

import com.app.helpdesk.model.Comment;

public interface CommentDAO {
    void save(Comment comment);
}
