package com.app.helpdesk.dao.impl;

import com.app.helpdesk.dao.HistoryDAO;
import com.app.helpdesk.model.History;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

import static org.springframework.transaction.annotation.Propagation.MANDATORY;

@Repository
@Transactional(propagation = MANDATORY)
public class HistoryDAOImpl implements HistoryDAO {

    private final EntityManager entityManager;

    @Autowired
    public HistoryDAOImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void save(History history) {
        entityManager.persist(history);
    }
}
