package com.app.helpdesk.listener;

import com.app.helpdesk.mail.impl.EmailGenerator;
import com.app.helpdesk.model.Ticket;
import com.app.helpdesk.model.enums.State;
import com.app.helpdesk.service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;

import static com.app.helpdesk.model.enums.State.DRAFT;

@Component
public class TicketListener {

    private static HistoryService historyService;
    private static EmailGenerator emailGenerator;

    @Autowired
    public void setHistoryService(final HistoryService historyService) {
        TicketListener.historyService = historyService;
    }

    @Autowired
    public void setEmailService(final EmailGenerator emailGenerator) {
        TicketListener.emailGenerator = emailGenerator;
    }

    @PostPersist
    void afterPersist(Ticket ticket) {
        historyService.saveAfterCreateTicket(ticket);
        if (ticket.getState() != DRAFT) {
            emailGenerator.resolveSendCase(ticket);
        }
    }

    @PostUpdate
    void afterUpdate(Ticket ticket) {
        State previousState = ticket.getPreviousStateHolder().getState();
        emailGenerator.resolveSendCase(ticket);
        if (previousState == ticket.getState()) {
            historyService.saveAfterUpdateTicket(ticket);
        } else {
            historyService.saveAfterUpdateState(previousState, ticket);
        }
    }
}
