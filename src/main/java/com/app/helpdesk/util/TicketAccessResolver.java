package com.app.helpdesk.util;

import com.app.helpdesk.dao.TicketDAO;
import com.app.helpdesk.exception.custom.NoAccessToChangeTicketStateException;
import com.app.helpdesk.exception.custom.NoAccessToTicketException;
import com.app.helpdesk.exception.custom.StateNotFoundException;
import com.app.helpdesk.exception.custom.TicketNotFoundException;
import com.app.helpdesk.model.Ticket;
import com.app.helpdesk.model.User;
import com.app.helpdesk.model.enums.Role;
import com.app.helpdesk.model.enums.State;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.Set;

import static com.app.helpdesk.model.enums.Role.*;
import static com.app.helpdesk.model.enums.State.*;

@Component
public class TicketAccessResolver {

    private final TicketDAO ticketDAO;

    private static final Map<Role, Set<State>> ACCESS_TO_CHANGE_STATE = Map.of(
            ROLE_EMPLOYEE, Set.of(NEW, CANCELLED),
            ROLE_MANAGER, Set.of(NEW, CANCELLED, APPROVED, DECLINED),
            ROLE_ENGINEER, Set.of(IN_PROGRESS, DONE)
    );

    @Autowired
    public TicketAccessResolver(TicketDAO ticketDAO) {
        this.ticketDAO = ticketDAO;
    }

    @Transactional(readOnly = true)
    public void checkAccessToDraftTicket(Long userId, Long ticketId) {
        ticketDAO.checkAccessToDraftTicket(userId, ticketId)
                .orElseThrow(() -> new NoAccessToTicketException("You can't have access to current ticket"));
    }

    @Transactional(readOnly = true)
    public void checkAccessToChangeTicketState(User user, Long ticketId, String newState) {

        Ticket ticket = getTicketForValidation(ticketId);

        if (ticket.getOwner().getEmail().equals(user.getEmail()) && ticket.getState() != DRAFT) {
            throw new NoAccessToTicketException("You can't formatted own ticket");
        }

        State state = getStateFromString(newState);

        boolean thereIsAccess = ACCESS_TO_CHANGE_STATE.get(user.getRole()).stream()
                .anyMatch(states -> states == state);

        if (!thereIsAccess) {
            throw new NoAccessToChangeTicketStateException("You can't change state current ticket");
        }
    }

    @Transactional(readOnly = true)
    public Ticket checkAccessToFeedbackTicket(User user, Long ticketId) {
        return ticketDAO.checkAccessToFeedbackTicket(user.getId(), ticketId)
                .orElseThrow(() -> new NoAccessToTicketException("You don't have access to current ticket"));
    }

    private State getStateFromString(String newState) {
        try {
            if (newState.contains("-")) {
                newState = newState.replace("-", "_");
            }
            return State.valueOf(newState.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new StateNotFoundException(String.format("This state \"%s\" does not exist", newState));
        }
    }

    private Ticket getTicketForValidation(Long ticketId) {
        return ticketDAO.getById(ticketId)
                .orElseThrow(() -> new TicketNotFoundException(String.format("Ticket with id %s not found", ticketId)));
    }
}
