package com.app.helpdesk.controller;

import com.app.helpdesk.dto.AuthenticationRequestDto;
import com.app.helpdesk.service.AuthenticationService;
import com.app.helpdesk.util.ValidatorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/login")
public class AuthenticationController {

    private final AuthenticationService authenticationService;
    private final ValidatorUtil validatorUtil;

    @Autowired
    public AuthenticationController(AuthenticationService authenticationService, ValidatorUtil validatorUtil) {
        this.authenticationService = authenticationService;
        this.validatorUtil = validatorUtil;
    }

    @PostMapping
    public ResponseEntity<?> login(@Valid @RequestBody AuthenticationRequestDto requestDto,
                                   BindingResult bindingResult) {
        List<String> errorMessages = validatorUtil.generateErrorMessages(bindingResult);

        if (!errorMessages.isEmpty()) {
            return new ResponseEntity<>(errorMessages, HttpStatus.UNAUTHORIZED);
        }
        Map<Object, Object> response = authenticationService.authenticateUser(requestDto);
        return ResponseEntity.ok(response);
    }
}
