package com.app.helpdesk.exception.custom;

public class NoAccessToChangeTicketStateException extends RuntimeException{
    public NoAccessToChangeTicketStateException(String message) {
        super(message);
    }
}
