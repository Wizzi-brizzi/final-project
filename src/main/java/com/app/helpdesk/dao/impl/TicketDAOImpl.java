package com.app.helpdesk.dao.impl;

import com.app.helpdesk.dao.TicketDAO;
import com.app.helpdesk.model.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

import static org.springframework.transaction.annotation.Propagation.MANDATORY;

@Repository
@Transactional(propagation = MANDATORY)
public class TicketDAOImpl implements TicketDAO {

    private static final String FILTER_BY = "(name like :filterRequest or desiredResolutionDate like :filterRequest or urgency like :filterRequest)";

    private final EntityManager entityManager;

    @Autowired
    public TicketDAOImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }


    @Override
    public List<Ticket> getForEmployee(Long employeeId, int amountTicketsAtPage, int page) {
        return entityManager.createQuery("from Ticket where owner.id = :id", Ticket.class)
                .setParameter("id", employeeId)
                .setFirstResult(amountTicketsAtPage * page)
                .setMaxResults(amountTicketsAtPage)
                .getResultList();
    }


    public long getAmountOwnForEmployee(Long employeeId) {
        return (long) entityManager.createQuery("select count(*) from Ticket where owner.id = :id")
                .setParameter("id", employeeId)
                .getSingleResult();
    }

    @Override
    public List<Ticket> getOwnForManager(Long managerId, int amountTicketsAtPage, int page) {
        return entityManager.createQuery("from Ticket where owner.id = :id", Ticket.class)
                .setParameter("id", managerId)
                .setFirstResult(amountTicketsAtPage * page)
                .setMaxResults(amountTicketsAtPage)
                .getResultList();
    }

    @Override
    public long getAmountOwnForManager(Long managerId) {
        return (long) entityManager.createQuery("select count(*) from Ticket where owner.id = :id")
                .setParameter("id", managerId)
                .getSingleResult();
    }

    @Override
    public List<Ticket> getAllForManager(Long managerId, int amountTicketsAtPage, int page) {
        return entityManager
                .createQuery("from Ticket where (state = 'NEW' and owner.role = 'ROLE_EMPLOYEE') or (approver.id = :id and state IN('APPROVED', 'DECLINED', 'CANCELED', 'IN_PROGRESS', 'DONE'))", Ticket.class)
                .setParameter("id", managerId)
                .setFirstResult(amountTicketsAtPage * page)
                .setMaxResults(amountTicketsAtPage)
                .getResultList();
    }

    @Override
    public long getAmountAllForManager(Long managerId) {
        return (long) entityManager.createQuery("select count(*) from Ticket where (state = 'NEW' and owner.role = 'ROLE_EMPLOYEE') or (approver.id = :id and state IN('APPROVED', 'DECLINED', 'CANCELED', 'IN_PROGRESS', 'DONE'))")
                .setParameter("id", managerId)
                .getSingleResult();
    }

    @Override
    public List<Ticket> getAllForEngineer(Long engineerId, int amountTicketsAtPage, int page) {
        return entityManager
                .createQuery("from Ticket where (state = 'APPROVED' and owner.role IN('ROLE_EMPLOYEE', 'ROLE_MANAGER')) or (assignee.id = :id and state IN('IN_PROGRESS', 'DONE'))", Ticket.class)
                .setParameter("id", engineerId)
                .setFirstResult(amountTicketsAtPage * page)
                .setMaxResults(amountTicketsAtPage)
                .getResultList();
    }

    @Override
    public long getAmountAllForEngineer(Long managerId) {
        return (long) entityManager.createQuery("select count(*) from Ticket where (state = 'APPROVED' and owned.role IN('ROLE_EMPLOYEE', 'ROLE_MANAGER')) or (assignee.id = :id and state IN('IN_PROGRESS', 'DONE'))")
                .setParameter("id", managerId)
                .getSingleResult();
    }

    @Override
    public List<Ticket> filterForManager(String filterRequest, Long managerId) {

        String hql = "from Ticket where (owner.id = :id or approver.id = :id) and " + FILTER_BY;

        return entityManager.createQuery(hql, Ticket.class)
                .setParameter("id", managerId)
                .setParameter("filterRequest", "%" + filterRequest + "%").getResultList();
    }

    @Override
    public List<Ticket> filterForEmployee(String filterRequest, Long employeeId) {

        String hql = "from Ticket where (owner.id = :id) and " + FILTER_BY;

        return entityManager.createQuery(hql, Ticket.class)
                .setParameter("id", employeeId)
                .setParameter("filterRequest", "%" + filterRequest + "%").getResultList();
    }

    @Override
    public List<Ticket> filterForEngineer(String filterRequest, Long engineerId) {

        String hql = "from Ticket where (assignee.id = :id) and " + FILTER_BY;

        return entityManager.createQuery(hql, Ticket.class)
                .setParameter("id", engineerId)
                .setParameter("filterRequest", "%" + filterRequest + "%").getResultList();
    }

    @Override
    public void save(Ticket ticket) {
        entityManager.persist(ticket);
    }

    @Override
    public void update(Ticket ticket) {
        entityManager.merge(ticket);
    }

    @Override
    public Optional<Ticket> getById(Long ticketId) {
        return entityManager.createQuery("from Ticket where id = :id", Ticket.class)
                .setParameter("id", ticketId)
                .getResultStream()
                .findAny();
    }

    @Override
    public Optional<Ticket> checkAccessToDraftTicket(Long userId, Long ticketId) {
        return entityManager.createQuery("from Ticket where owner.id = :userId and id = :id and state = 'DRAFT'", Ticket.class)
                .setParameter("userId", userId)
                .setParameter("id", ticketId)
                .getResultStream()
                .findAny();
    }

    @Override
    public Optional<Ticket> checkAccessToFeedbackTicket(Long userId, Long ticketId) {
        return entityManager.createQuery("from Ticket where owner.id = :userId and id = :id and state = 'DONE'", Ticket.class)
                .setParameter("userId", userId)
                .setParameter("id", ticketId)
                .getResultStream()
                .findAny();
    }

    @Override
    public void changeState(Ticket ticket) {
        entityManager.merge(ticket);
    }
}
