package com.app.helpdesk.util;

import com.app.helpdesk.dao.AttachmentDAO;
import com.app.helpdesk.exception.custom.NoAccessToAttachmentDeleteException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class AttachmentAccessResolver {

    private final AttachmentDAO attachmentDAO;

    @Autowired
    public AttachmentAccessResolver(AttachmentDAO attachmentDAO) {
        this.attachmentDAO = attachmentDAO;
    }

    @Transactional(readOnly = true)
    public void checkAccessToDeleteAttachment(Long userId, Long attachmentId) {
        attachmentDAO.getForUserById(userId, attachmentId)
                .orElseThrow(() -> new NoAccessToAttachmentDeleteException("You can't delete this attachment"));
    }
}
