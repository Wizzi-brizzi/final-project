package com.app.helpdesk.mail.impl;

import com.app.helpdesk.mail.EmailService;
import com.app.helpdesk.mail.MailSendState;
import com.app.helpdesk.model.Feedback;
import com.app.helpdesk.model.Ticket;
import com.app.helpdesk.model.enums.State;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Consumer;

import static com.app.helpdesk.mail.MailSendState.*;
import static com.app.helpdesk.model.enums.State.NEW;

@Service
public class EmailGenerator {

    private final Map<MailSendState, Consumer<Long>> sendStatus;
    private final EmailService emailService;

    @Autowired
    public EmailGenerator(EmailService emailService) {
        sendStatus = Map.of(
                NEW_NEW, emailService::sendNewTicketMail,
                DRAFT_NEW, emailService::sendNewTicketMail,
                DECLINED_NEW, emailService::sendNewTicketMail,
                NEW_APPROVED, emailService::sendApprovedTicketMail,
                NEW_CANCELLED, emailService::sendNewCancelledTicketMail,
                NEW_DECLINED, emailService::sendDeclinedTicketMail,
                APPROVED_CANCELLED, emailService::sendApprovedCancelledTicketMail,
                IN_PROGRESS_DONE, emailService::sendDoneTicketMail
        );
        this.emailService = emailService;
    }

    @Async("threadPoolTaskExecutor")
    public void resolveSendCase(Ticket ticket) {
        Map<State, State> ticketState = Map.of(
                ticket.getPreviousStateHolder() == null ? NEW : ticket.getPreviousStateHolder().getState(),
                ticket.getState()
        );
        Arrays.stream(MailSendState.values())
                .filter(states -> states.isPreviousStateToCurrentStateMapEqualTo(ticketState))
                .findFirst()
                .ifPresent(state -> sendStatus.get(state).accept(ticket.getId()));
    }

    @Async("threadPoolTaskExecutor")
    public void sendFeedbackNotification(Feedback feedback) {
        emailService.sendFeedback(feedback.getTicket().getId());
    }
}
