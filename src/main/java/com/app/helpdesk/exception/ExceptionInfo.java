package com.app.helpdesk.exception;

import lombok.Data;

@Data
public class ExceptionInfo {
    private String info;
}
