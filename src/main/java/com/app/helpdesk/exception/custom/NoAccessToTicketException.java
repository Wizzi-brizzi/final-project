package com.app.helpdesk.exception.custom;

public class NoAccessToTicketException extends RuntimeException{
    public NoAccessToTicketException(String message) {
        super(message);
    }
}
