package com.app.helpdesk.exception.custom;

public class AttachmentNotFoundException extends RuntimeException{
    public AttachmentNotFoundException(String message) {
        super(message);
    }
}
