package com.app.helpdesk.controller;

import com.app.helpdesk.dto.FilterTicketDto;
import com.app.helpdesk.dto.TicketDto;
import com.app.helpdesk.dto.TicketDtoWrapper;
import com.app.helpdesk.model.enums.State;
import com.app.helpdesk.security.CustomUserDetails;
import com.app.helpdesk.service.TicketService;
import com.app.helpdesk.util.TicketAccessResolver;
import com.app.helpdesk.util.ValidatorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/tickets")
public class TicketController {

    private static final String URI_FOR_RESPONSE = "/tickets/change-state/%s";

    private final TicketService ticketService;
    private final ValidatorUtil validatorUtil;
    private final TicketAccessResolver ticketAccessResolver;

    @Autowired
    public TicketController(TicketService ticketService, ValidatorUtil validatorUtil, TicketAccessResolver ticketAccessResolver) {
        this.ticketService = ticketService;
        this.validatorUtil = validatorUtil;
        this.ticketAccessResolver = ticketAccessResolver;
    }

    @GetMapping("/all")
    public ResponseEntity<TicketDtoWrapper> showAll(@AuthenticationPrincipal CustomUserDetails userDetails,
                                                    @RequestParam(name = "sort", required = false, defaultValue = "null") String sort,
                                                    @RequestParam(name = "amountTickets", required = false, defaultValue = "5") Integer amountTicketsAtPage,
                                                    @RequestParam(name = "page", required = false, defaultValue = "0") Integer page) {
        TicketDtoWrapper ticketDtoWrapper = ticketService.getAllTicketsForCurrentUser(userDetails.getUser(), sort, amountTicketsAtPage, page);
        return ResponseEntity.ok(ticketDtoWrapper);
    }

    @GetMapping("/my")
    public ResponseEntity<TicketDtoWrapper> showMy(@AuthenticationPrincipal CustomUserDetails userDetails,
                                                   @RequestParam(name = "sort", required = false, defaultValue = "null") String sort,
                                                   @RequestParam(name = "amountTickets", required = false, defaultValue = "5") Integer amountTicketsAtPage,
                                                   @RequestParam(name = "page", required = false, defaultValue = "0") Integer page) {
        TicketDtoWrapper ticketDtoWrapper = ticketService.getOwnTicketsForCurrentUser(userDetails.getUser(), sort, amountTicketsAtPage, page);
        return ResponseEntity.ok(ticketDtoWrapper);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TicketDto> showCurrent(@PathVariable Long id) {
        TicketDto ticketById = ticketService.getTicketDtoById(id);
        return ResponseEntity.ok(ticketById);
    }

    @GetMapping("/{id}/draft")
    public ResponseEntity<TicketDto> getForDraft(@AuthenticationPrincipal CustomUserDetails userDetails,
                                                 @PathVariable Long id) {
        ticketAccessResolver.checkAccessToDraftTicket(userDetails.getUser().getId(), id);
        TicketDto draftTicket = ticketService.getDraft(id);
        return ResponseEntity.ok(draftTicket);
    }

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<List<String>> create(@Valid @RequestPart("jsonBodyData") TicketDto ticketDto,
                                               BindingResult bindingResult,
                                               @RequestPart(name = "uploadFile", required = false) MultipartFile file,
                                               @AuthenticationPrincipal CustomUserDetails userDetails,
                                               @RequestParam(value = "save", required = false) String draft) {
        List<String> fileUploadErrors = validatorUtil.validateUploadFile(file);
        List<String> errorMessages = validatorUtil.generateErrorMessages(bindingResult);

        if (isErrorsPresents(fileUploadErrors, errorMessages)) {
            errorMessages.addAll(fileUploadErrors);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMessages);
        }
        ticketService.saveOrUpdate(ticketDto, file, userDetails.getUser(), draft);
        return ResponseEntity.created(URI.create("/tickets")).build();
    }

    @PutMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<List<String>> update(@Valid @RequestPart("jsonBodyData") TicketDto ticketDto,
                                               BindingResult bindingResult,
                                               @RequestPart(name = "uploadFile", required = false) MultipartFile file,
                                               @AuthenticationPrincipal CustomUserDetails userDetails,
                                               @RequestParam(value = "save", required = false) String draft) {
        List<String> fileUploadErrors = validatorUtil.validateUploadFile(file);
        List<String> errorMessages = validatorUtil.generateErrorMessages(bindingResult);

        if (isErrorsPresents(fileUploadErrors, errorMessages)) {
            errorMessages.addAll(fileUploadErrors);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMessages);
        }
        ticketService.saveOrUpdate(ticketDto, file, userDetails.getUser(), draft);
        return ResponseEntity.created(URI.create("/tickets")).build();
    }

    private boolean isErrorsPresents(List<String> fileUploadErrors, List<String> errorMessage) {
        return !fileUploadErrors.isEmpty() || !errorMessage.isEmpty();
    }

    @PostMapping("/filter")
    public ResponseEntity<?> filterTickets(@AuthenticationPrincipal CustomUserDetails userDetails,
                                           @Valid @RequestBody FilterTicketDto data,
                                           BindingResult bindingResult) {
        List<String> errorMessages = validatorUtil.generateErrorMessages(bindingResult);
        if (!errorMessages.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMessages);
        }
        List<TicketDto> ticketsDto = ticketService.filter(userDetails.getUser(), data.getRequest());
        return ResponseEntity.ok(ticketsDto);
    }

    @PutMapping("/{ticketId}/change-state")
    public ResponseEntity<?> changeTicketsState(@AuthenticationPrincipal CustomUserDetails userDetails,
                                                @PathVariable Long ticketId,
                                                @RequestParam(value = "new-state", required = false) String newState) {
        ticketAccessResolver.checkAccessToChangeTicketState(userDetails.getUser(), ticketId, newState);
        ticketService.changeState(userDetails.getUser(), ticketId, State.valueOf(newState.toUpperCase()));
        return ResponseEntity.created(URI.create(String.format(URI_FOR_RESPONSE, ticketId))).build();
    }
}
