package com.app.helpdesk.dao;

import com.app.helpdesk.model.Ticket;

import java.util.List;
import java.util.Optional;

public interface TicketDAO {
    List<Ticket> getForEmployee(Long employeeId, int amountTicketsAtPage, int page);

    long getAmountOwnForEmployee(Long employeeId);

    List<Ticket> getOwnForManager(Long managerId, int amountTicketsAtPage, int page);

    long getAmountOwnForManager(Long managerId);

    List<Ticket> getAllForManager(Long managerId, int amountTicketsAtPage, int page);

    long getAmountAllForManager(Long managerId);

    List<Ticket> getAllForEngineer(Long engineerId, int amountTicketsAtPage, int page);

    long getAmountAllForEngineer(Long managerId);

    List<Ticket> filterForManager(String filterRequest, Long managerId);

    List<Ticket> filterForEmployee(String filterRequest, Long employeeId);

    List<Ticket> filterForEngineer(String filterRequest, Long engineerId);

    void save(Ticket ticket);

    void update(Ticket ticket);

    Optional<Ticket> getById(Long ticketId);

    Optional<Ticket> checkAccessToDraftTicket(Long userId, Long ticketId);

    Optional<Ticket> checkAccessToFeedbackTicket(Long userId, Long ticketId);

    void changeState(Ticket ticket);
}
