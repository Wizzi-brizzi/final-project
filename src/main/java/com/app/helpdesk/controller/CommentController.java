package com.app.helpdesk.controller;

import com.app.helpdesk.dto.CommentDto;
import com.app.helpdesk.security.CustomUserDetails;
import com.app.helpdesk.service.CommentService;
import com.app.helpdesk.util.ValidatorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/comments")
public class CommentController {

    private static final String URI_FOR_RESPONSE = "/comments/%s";

    private final CommentService commentService;
    private final ValidatorUtil validatorUtil;


    @Autowired
    public CommentController(CommentService commentService, ValidatorUtil validatorUtil) {
        this.commentService = commentService;
        this.validatorUtil = validatorUtil;
    }

    @PostMapping("/{ticketId}")
    public ResponseEntity<?> createNew(@Valid @RequestBody CommentDto commentDto,
                                       BindingResult bindingResult,
                                       @PathVariable Long ticketId,
                                       @AuthenticationPrincipal CustomUserDetails userDetails) {
        List<String> errorMessages = validatorUtil.generateErrorMessages(bindingResult);
        if (!errorMessages.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMessages);
        }
        commentService.save(userDetails.getUser(), ticketId, commentDto);
        return ResponseEntity.created(URI.create(String.format(URI_FOR_RESPONSE, ticketId))).build();
    }
}
