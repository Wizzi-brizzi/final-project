package com.app.helpdesk.listener;

import com.app.helpdesk.mail.impl.EmailGenerator;
import com.app.helpdesk.model.Feedback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.PostPersist;

@Component
public class FeedbackListener {

    private static EmailGenerator emailGenerator;

    @Autowired
    public void setEmailService(final EmailGenerator emailGenerator) {
        FeedbackListener.emailGenerator = emailGenerator;
    }

    @PostPersist
    void afterPersist(Feedback feedback) {
        emailGenerator.sendFeedbackNotification(feedback);
    }
}
